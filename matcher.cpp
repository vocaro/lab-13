#include <iostream>

using namespace std;

class CharStack {
private:
    char *stack;
    int max;
    int size = 0;
public:
    CharStack(int newMax) {
        max = newMax;
        stack = new char[max];
    }
    ~CharStack() {
        delete [] stack;
    }
    bool push(char c) {
        if (size == max) {
            return false;
        }
        
        stack[size++] = c;
        
        return true;
    }
    char pop() {
        if (size == 0) {
            return 0;
        }
        return stack[--size];
    }
    bool isEmpty() const {
        return size == 0;
    }
};

class PairMatcher {
private:
    char _openChar, _closeChar;
    CharStack charStack;
public:
    PairMatcher(char openChar, char closeChar) : charStack(100) {
        _openChar = openChar;
        _closeChar = closeChar;
    }
    bool check(const string &testString) {
        for (int i = 0; i < testString.length(); i++) {
            if (testString[i] == _openChar) {
                charStack.push(_openChar);
            } else if (testString[i] == _closeChar) {
                if (charStack.pop() != _openChar) {
                    return false;
                }
            }
        }
        return charStack.isEmpty();
    }
};

int main() {
    PairMatcher matcher('(', ')');
    
    string testString = "((())()";
    
    cout << testString << " is " << (matcher.check(testString) ? "valid" : "invalid") << endl;
    return 0;
}
